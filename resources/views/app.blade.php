<!doctype html>
<html lang="en">
<head>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-8962379120623174",
            enable_page_level_ads: true
        });
    </script>
    <meta charset="UTF-8">
    <title>@yield('title')</title>

    <link rel="stylesheet" href="/css/bootstrap.min.css">

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" />

    <link rel="stylesheet" href="/css/main.css" />

    <link href='https://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic,cyrillic-ext' rel='stylesheet' type='text/css'>

    <meta name="description" content="@yield('meta_description')">
    <meta name="keywords" content="@yield('meta_keywords')">

    @yield('head_end')
</head>
<body>

    @include('partials.nav')

    <div id="content">
        @include('flash::message')
        <div class="container">
            @yield('content')
        </div>
    </div>


    @yield('footer')
    <footer class="footer">
        <div class="footer-menu">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <a href="/" title="Павел Рыбаков">Павел Рыбаков - senior web/java/php developer</a>
                        <p>{{ Inspiring::quote() }}</p>
                    </div>
                    <div class="col-md-6">
                        <span class="pull-right">
                            <a href="mailto:&#112;&#097;&#118;&#101;&#108;&#046;&#114;&#121;&#098;&#097;&#107;&#111;&#064;&#103;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;">&#112;&#097;&#118;&#101;&#108;&#046;&#114;&#121;&#098;&#097;&#107;&#111;&#064;&#103;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;</a><br/>
                            <a href="https://www.facebook.com/pavel.rybako" target="_blank">Facebook: Pavel Rybakov</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script src="//code.jquery.com/jquery.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

    <script>$('#flash-overlay-modal').modal();</script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-29199759-6', 'auto');
        ga('send', 'pageview');
    </script>

    @yield('bodyend')
</body>
</html>