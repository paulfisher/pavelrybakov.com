@extends('app')

@section('content')

<h1>About me</h1>
<p>
    My name is Pavel and I'm a professional software developer with 11 years of experience in backend software development.
    <br/>
    If you need a help with any software development or maybe you want to offer me a job, please contact me via <a href="https://www.facebook.com/pavel.rybako" target="_blank">Facebook</a> or <a href="https://www.linkedin.com/in/pavelrybako" target="_blank">LinkedIn</a>
    <br/>
    Here is a list of my web-apps, happy customers and positions I worked at.
</p>

<hr>
@foreach($cv['extra'] as $proj)
    <p>
        @if (isset($proj['website']))
            <noindex>
                <h4>
                    <a href="{{$proj['website']}}" target="_blank" rel="nofollow">{{$proj['name']}}</a>
                </h4>
            </noindex>
        @else
            <h4>
                {{$proj['name']}}
            </h4>
        @endif
        {{$proj['content']}}
    </p>
    <hr>
@endforeach

@foreach($cv['jobs'] as $job)
    <p>
        @if (isset($job['website']))
            <noindex>
                <h4>
                    <a href="{{$job['website']}}" target="_blank" rel="nofollow">{{$job['employer']}}</a>
                    <span class="color-gray">{{$job['date']}}</span>
                </h4>
            </noindex>
        @else
            <h4>
                {{$job['employer']}}
                <span class="color-gray">{{$job['date']}}</span>
            </h4>
        @endif
        <p>
            {{$job['content']}}
        </p>
        <p>
            @foreach($job['tech'] as $tech)
                <span>{{$tech}}</span>
            @endforeach
            @if (!empty($job['extraTech']))
            and
            @foreach($job['extraTech'] as $tech)
                <span>{{$tech}}</span>
            @endforeach
            @endif
        </p>
    <hr>
    </p>
@endforeach

@stop