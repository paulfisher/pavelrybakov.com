<html>
<head>
    <link rel="stylesheet" href="/css/flipclock.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic,cyrillic-ext' rel='stylesheet' type='text/css'>
    <style>
        body {
            margin: 0;
            width: 100%;
            color: #dbecf8;
            display: table;
            font-weight: 100;
            font-family: 'Open Sans', sans-serif;
        }
        html {
            background: url(/images/travel.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='.myBackground.jpg', sizingMethod='scale');
            -ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='myBackground.jpg', sizingMethod='scale')";
        }
        a { color: #dbecf8; }
        .center {
            position: absolute;
            top: 50%;
            left: 50%;
            margin-right: -50%;
            transform: translate(-50%, -50%);
        }
        p {
            width: 280px;
            font-size: small;
        }
    </style>
    <title>Майский сплав 2016</title>
</head>
<body>

<div id="rafting-about">
    <p>
        С начала весны у каждого уважающего себя походника начинается сладкая пора. Подготовка к майскому сплаву.
        Закупка снаряги, поиск сплавсредств, планирование провизии, трансферов.. Быть частью этого
        кипиша - каждый раз радость:) Запилил простенький счётчик. Для медитации конечно.
        <br/>Обратный отсчет запущен. Встретимся 1 мая на реках нашей Родины:)
    </p>
    <h3>Мир, труд, сплав, товарищи!</h3>
    <a href="http://pavelrybakov.com/article/mayskiy-splav-2015-bolshaya-kokshaga-mariy-el" title="Наш сплав по Большой Кокшаге" target="_blank">
        <small>Наш сплав по Б. Кокшаге 2015</small>
    </a>
    <br/>
    <a href="javascript: $('#rafting-about').remove();" style="font-size: 10px;">скрыть текст</a>
</div>

<div class="center">
    <div class="rafting-countdown"></div>
</div>

<script src="/js/lib/jquery-1.12.2.min.js"></script>
<script src="/js/lib/flipclock.min.js"></script>
<script>

    var t1 = new Date(2016, 4, 1);
    var t2 = new Date();
    var dif = (t1 - t2) / 1000;
    var clock = $('.rafting-countdown').FlipClock(dif, {
        countdown: true,
        clockFace: 'DailyCounter'
    });
    clock.start();
</script>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-29199759-6', 'auto');
    ga('send', 'pageview');
</script>
</body>
</html>