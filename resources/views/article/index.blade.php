@extends('app')

@section('meta_description') Слабоумие и отвага. Путешествия по Таиланду и баскетбольные площадки @endsection
@section('meta_keywords') Путешествия,Камбоджа,Таиланд,баскетбол,тренажерные залы,Чианг Май,Паттайя,Самуи @endsection


@section('content')
    <div class="row">
        <div class="col-sm-8">
            <h3>Random</h3>
            <div class="well">
                @foreach ($articles as $article)
                    <div class="row article-item">
                        <div class="col-sm-12">
                            <article>
                                <div class="pull-left article-item-main_img">
                                    @if ($article->main_img)
                                        <a href="{{url('/article', $article->slugOrId()) }}" title="{{$article->title}}">
                                            <img src="{!! $article->main_img !!}" width="150" alt="{{$article->title}}"/>
                                        </a>
                                    @endif
                                </div>
                                <div class="pull-left article-item-info">
                                    <div class="article-item-time text-muted">
                                        <span>{{$article->created_at->diffForHumans()}}</span>
                                    </div>
                                    <div class="article-item-link">
                                        <a href="{{url('/article', $article->slugOrId()) }}" title="{{$article->title}}">
                                            {{$article->title}}
                                        </a>
                                    </div>
                                    <div class="article-item-short">
                                        {!! $article->short !!}
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                    <hr/>
                @endforeach
            </div>
        </div>
        <div class="col-sm-4">
            {{--<ul>
                <li><a href="http://tintinmusic.ru" target="_blank" title="Музыкальные сборники на любой случай онлайн">Слушать музыку онлайн</a></li>
            </ul>--}}

            {{--<div id="spoken-container"></div>
            <script>
                var script = document.createElement("script");
                script.src = "http://spoken.loc/js/embed.js?test=" + Math.random();
                script.onload = function() {
                    SPOKEN.embed({containerId:"spoken-container", loadFrom: 'spoken.loc', debug: true, style: 'gray-light'})
                };
                document.head.appendChild(script);
            </script>--}}

            <!-- responsive common -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-8962379120623174"
                 data-ad-slot="5067132044"
                 data-ad-format="auto"></ins>
            <script>(adsbygoogle = window.adsbygoogle || []).push({});</script>

        </div>
    </div>

@stop