<style>
    .form-group label {
        color: #3a3f58;
        font-size: 14px;
    }
</style>
<div class="form-group">
    {!! Form::label('image', 'Главное изображение:') !!}
    @if ($article->main_img)
        <div>
            <img src="{!! $article->main_img !!}" width="200"/>
        </div>
    @endif
    {!! Form::file('image', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('title', 'Заголовок:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('short', 'Короткое описание:') !!}
    {!! Form::text('short', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('slug', 'Url статьи:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
</div>

<div class="form-group">
    {!! Form::label('body', 'Тело статьи:') !!}
    {!! Form::textarea('body', null, ['class' => 'form-control', 'id' => 'article-body']) !!}
</div>

<div class="form-group">
    {!! Form::label('published_at', 'Когда опубликовать:') !!}
    {!! Form::input('date', 'published_at', $article->published_at, ['class' => 'form-control']) !!}
</div>


<div class="form-group">
    {!! Form::label('tag_list', 'Тэги:') !!}
    {!! Form::select('tag_list[]', App\Tag::lists('name','id'), null, ['id' => 'tag_list', 'class' => 'form-control', 'multiple' => true]) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
</div>


@section('footer')
<script>
    $('#tag_list').select2({
        placeholder: 'Выберите тэг'
    });
</script>

<script src="/js/lib/ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace('article-body');
</script>
@endsection