@extends('app')

@section('content')

    <a href="{{route('article.show', $article->id)}}">&larr; </a>

    <h1>Редактирование поста "{!! $article->title !!}"  </h1>


    {!! Form::model($article, ['method' => 'PATCH', 'files' => true, 'action' => ['ArticleController@update', $article->id, 'files' => true]]) !!}
        @include('article.form', ['submitButtonText' => 'Обновить'])
    {!! Form::close() !!}


    @include('errors.list')

@stop