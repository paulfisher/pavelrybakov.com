@extends('app')

@section('title') Travel bag security kit polling @endsection

@section('meta_description') Travel bag security kit @endsection
@section('meta_keywords') Travel bags, luggage, smart bags, security @endsection

@section('content')


    <div class="row">
        <div class="col-md-12">
            <p>
                Redirecting...<br/>
                If poll is not open, <a href="https://qtrial2016q1az1.az1.qualtrics.com/jfe/form/SV_eCIyRlJpUgxv673" target="_blank" class="btn btn-default">click here</a>
            </p>
            <script>
                document.location.href = "https://qtrial2016q1az1.az1.qualtrics.com/jfe/form/SV_eCIyRlJpUgxv673";
            </script>
        </div>
    </div>
@stop