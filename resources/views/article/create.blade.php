@extends('app')

@section('content')
    <h1>Ща я выдам крутой пост</h1>

    <hr/>

    {!! Form::model($article = new \App\Article, ['url' => 'article', 'files' => true]) !!}
        @include('article.form', ['submitButtonText' => 'Сохранить'])
    {!! Form::close() !!}


    @include('errors.list')

@stop