@extends('app')
@section('title'){{$article->title}} @endsection

@section('meta_description'){{$article->title}} @endsection
@section('meta_keywords')Путешествия,Камбоджа,Таиланд,баскетбол,тренажерные залы,Чианг Май,Паттайя,Самуи,{{$article->title}} @endsection

@section('head_end')
    <script type="text/javascript" async src="https://relap.io/api/v6/head.js?token=IVkh8XXxodEsAeDl"></script>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li><a href="{{url('/')}}" title="Путешествия Павла Рыбакова">Домой</a></li>
                <li class="active">{{$article->title}}</li>
            </ol>

            @if (Auth::check() && $article->user->id == Auth::user()->id)
                <a href="{{route('article.edit', $article->id)}}" class="pull-right btn btn-lg btn-primary">редактировать</a>
            @endif

            <div class="page-header">
                <h1>{{$article->title}}</h1>
                @if ($article->main_img)
                <div class="row">
                    <div class="col-md-8">
                        <img src="{!! $article->main_img !!}" style="max-width: 800px;" alt="{{$article->title}}" class="img-responsive"/>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <article class="article-text">
                    {!! $article->body !!}
                </article>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <script id="l2iXp2ena8Y84NjY">if (window.relap) window.relap.ar('l2iXp2ena8Y84NjY');</script>
        </div>
    </div>

    <hr/>

    @unless ($article->tags->isEmpty())
        <ul>
            @foreach ($article->tags->lists('name') as $tag)
                <li>{{$tag}}</li>
            @endforeach
        </ul>
    @endunless

    <!-- LikeBtn.com BEGIN -->
    <span class="likebtn-wrapper" data-theme="padded"></span>
    <script>(function(d,e,s){if(d.getElementById("likebtn_wjs"))return;a=d.createElement(e);m=d.getElementsByTagName(e)[0];a.async=1;a.id="likebtn_wjs";a.src=s;m.parentNode.insertBefore(a, m)})(document,"script","//w.likebtn.com/js/w/widget.js");</script>
    <!-- LikeBtn.com END -->

    <div id="disqus_thread"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES * * */
        var disqus_shortname = 'pavelrybakov';

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
@stop

@section('bodyend')
    <script>
        $(function() {
            $('article img').each(function() {
                var $this = $(this);
                $this.attr('title', 'Открыть в полном размере в новом окне');
                $this.addClass('img-responsive');
                $this.bind('click', function() {
                    var originalSrc = $this.attr('src')
                            .replace(/_\[w_\d+\]/, '');
                    window.open(originalSrc, '_blank');
                });
            });
        });
    </script>

@stop