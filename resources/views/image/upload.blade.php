{!! Form::open(['url' => 'image/upload', 'files' => true]) !!}
    {!! Form::file('image[]', ['multiple', 'class' => 'form-control', 'id' => 'image']) !!}
    {!! Form::hidden('callbackId', $callbackId) !!}
    {!! Form::submit("upload", ['class' => 'btn btn-primary form-control', 'id' => 'btn-submit']) !!}
{!! Form::close() !!}


<script>
    document.getElementById('image').onchange = function() {
        document.getElementById('btn-submit').click();
    };
</script>
