<nav class="navbar navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                Pavel Rybakov - Software development
            </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">

            </ul>
            @if (Auth::check())
            <ul class="nav navbar-nav navbar-right">
                <li><a href="https://www.facebook.com/pavel.rybako" target="_blank">Contact</a></li>
                <li><a href="/">{{ Auth::user()->name }}</a></li>
                <li><a href="{{route('article.create')}}" title="добавить статью">Добавить статью</a></li>
                <li>{!! link_to('auth/logout', 'Выйти') !!}</li>
            </ul>

            @else
            <ul class="nav navbar-nav navbar-right">
                <li>
                    {!! link_to('blog', 'Blog') !!}
                </li>
                <li>
                    <a href="https://www.facebook.com/pavel.rybako" target="_blank">Contact me</a>
                </li>
            </ul>
            @endif
        </div>

    </div>
</nav>

{{--
<script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'ru', includedLanguages: 'de,en,es,th,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, multilanguagePage: true, gaTrack: true, gaId: 'UA-29199759-6'}, 'google_translate_element');
    }
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>--}}
