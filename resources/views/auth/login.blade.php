@extends('app')



@section('content')

<div class="col-md-6 col-md-offset-3">
    <h1>Login</h1>

    <div class="form">
        @if (count($errors) > 0)
        <div class="alert alert-warning">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <form method="POST" action="{!! url('auth/login') !!}">
            {!! csrf_field() !!}

            <div class="form-group">
                {!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email']) !!}
            </div>

            <div class="form-group">
                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
            </div>

            <div class="form-group">
                <label><input type="checkbox" name="remember">Remember Me</label>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control">Login</button>
            </div>
        </form>
    </div>

    {!! link_to('auth/register', 'Join') !!}
</div>


@endsection
