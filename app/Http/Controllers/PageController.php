<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function index() {
        $cv = [
            'jobs' => [
            [
                'website' => 'https://politico.com',
                'employer' => 'Nitka/Politico',
                'title' => 'Senior Java Developer',
                'date' => '2016 Oct. - current time',
                'content' => 'Microservices development on Spring Boot. My everyday tasks - backend API development,
development and maintenance of commonly used libraries. Also I\'ve been working on the CMS
development for the main web-site politico.com',
                'tech' => ['Spring Boot 1.x/2.x', 'Kafka', 'MySQL'],
                'extraTech' => ['php(Symphony)', 'js', 'Brightspot CMS']
            ],
            [
                'employer' => 'Age Of Fury 3D',
                'title' => 'Senior Java developer',
                'date' => '2014 Feb. - 2014 Nov.',
                'content' => 'IOS game Age Of Fury backend development. Came on board to help with SmartFox server which
was hardly able to handle 100 players online. I\'ve proposed a solution to optimize performance of
the server. Implementation of this solution allowed us to handle thousands of players online.',
                'tech' => ['SmartFoxServer/Java', 'PostgreSQL'],
                'extraTech' => []
            ],
            [
                'website' => 'https://travelline.ru',
                'employer' => 'TravelLine',
                'title' => 'Senior C# developer',
                'date' => '2009 Aug. - 2014 Jan.',
                'content' => 'Joined the team being a second grade student. Took part in development of the most parts of the
system, starting with external iframe integration of the Travelline module ending core booking logic
development. During 4 years I went the way from junior developer to the senior developer. Have
developed B2B part for agents - special tariffs/commissions.',
                'tech' => ['C#', '.NET', 'MSSQL', 'php', 'js', 'MySQL'],
                'extraTech' => ['java']
            ],
        ],
            'extra' => [
                [
                    'name' => 'Airport-Way.com',
                    'website' => 'https://Airport-Way.com',
                    'content' => 'online transfer booking engine. this booking system allows to start up a airport-to-city transfer business easily',
                    'tech' => ['Laravel', 'js', 'php', 'MySQL']
                ],
                [
                    'name' => 'BackItForward.com(now kickico.com)',
                    'content' => 'crowdfunding platform with very complicated logic. currently transformed into ICO based crowdfunding platform by dev company',
                    'tech' => ['Yii', 'Laravel', 'js', 'php', 'MySQL']
                ],
                [
                    'name' => 'TinTinMusic.ru',
                    'website' => 'http://tintinmusic.ru',
                    'content' => 'my private online audio player, used to use it to listen my favorite playlists',
                    'tech' => ['Laravel', 'js', 'php', 'MySQL']
                ],
                [
                    'name' => 'rfidcontrol.ru',
                    'content' => 'RFID-tag based logistic system for warehouses. I\'ve developed 3 separate parts of the system: backend server (java, jersey), admin/manager control panel(AngularJs) and android application with RFID scanner embedded',
                    'tech' => ['java', 'Jersey', 'PostgreSQL', 'android', 'AngularJs']
                ]
            ]
        ];

        return view('pages.index', compact('cv'));
    }

    public function travel() {
        $articles = [
            [
                'name' => 'Пхукет',
                'items' => [
                    '/article/basketbolnaya-ploschadka-na-phukete-phuket-town-basketball-courts' => 'Баскетбол на Пхукете',
                    '/article/11' => 'Пхукет таун, фитнес парк',
                    '/article/phuket-moe-rabochee-mesto-my-workplace-phuket-town' => 'Пхукет, мое рабочее место',
                    '/article/kak-doehat-iz-phuketa-do-pattayi' => 'Как доехать из Пхукета до Паттайи'
                ],
            ],
            [
                'name' => 'Паттайя',
                'items' => [
                    '/article/pattayya-basketbol-v-lumpini-jomtien' => 'Баскетбол в Lumpini Jomtien',
                    '/article/pattayya-basketbol-u-sema-pattaya-basketball-at-sam-s' => 'Баскетбол у Сэма',
                    '/article/rpod-coworking-naklua-pattaya' => 'Коворкинг в Паттайе',
                ],
            ],
            [
                'name' => 'Сиануквиль',
                'items' => [
                    '/article/phuket-sianukvil-pereezd' => 'Дорога в Сиануквиль',
                    '/article/nemnogo-o-sianukvile-sihanoukville-photos' => 'Немного о Сиануквиле',
                    '/article/sianukvil-plyusy-i-minusy' => 'Плюсы и минусы Сиануквиля',
                    '/article/trenazhernyy-zal-v-sianukvile' => 'Тренажерка в Сиануквиле',
                ],
            ],
            [
                'name' => 'Самуи',
                'items' => [
                    '/article/dekabr-2015-laos-pattayya-samui' => 'Дорога на Самуи',
                    '/article/samui-plyazh-lipanoy' => 'Пляж Липа Ной',
                    '/article/novyy-god-2016' => 'Новый год на Самуи',
                    '/article/samui-poezdka-po-goram' => 'Поездка по джунглям на мотобайках',
                ],
            ],
            [
                'name' => 'Чианг Май',
                'items' => [
                    '/article/samui-chiang-may-pereezda-samui-bangkok' => 'Дорога в Чианг Май',
                    '/article/progulka-po-chiang-mayu' => 'Прогулка по Чианг Маю',
                    '/article/basketbol-v-chiang-mae-basketball-court-in-chiang-mai-university' => 'Баскетбольный корт университета Чианг Мая',
                    '/article/chiang-may-hram-doy-suttep-chiang-mai-wat-doi-suthep' => 'Храм Дой Суттеп',
                    '/article/basketball-court-in-yupparaj-school-chiang-mai-basketbol-v-chiang-mae' => 'Баскетбольный корт в старом городе Чианг Мая',
                    '/article/gym-in-chiang-mai-trenazhernyy-zal-v-chiang-mae' => 'Тренажерный зал GoGym в Чианг Мае',
                    '/article/vecherniy-chiang-may-rynok' => 'Прогулка по Чианг Маю',
                ],
            ]
        ];
        return view('pages.travel', compact('articles'));
    }

    public function rafting2016() {
        return view('pages.rafting-2016');
    }
}
