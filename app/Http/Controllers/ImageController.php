<?php

namespace App\Http\Controllers;

use App\Repositories\ImageRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageController extends Controller {

    public function getUpload(Request $request) {
        $callbackId = $request->get('CKEditorFuncNum', 1);
        return view('image.upload', compact('callbackId'));
    }

    public function postUpload(Request $request, ImageRepository $imageRepository) {
        set_time_limit(0);
        $images = [];
        $files = Input::file('image');
        try {
            foreach($files as $file) {
                /** @var UploadedFile $file */
                if ($file->isValid()) {
                    $images[] = $imageRepository->storeImage($file);
                }
            }
        } catch(\Exception $e) {
            return view('image.upload', ['callbackId' => 1]);
        }

        $callbackId = $request->get('callbackId');
        return view('image.uploaded', compact('callbackId', 'images'));
    }

    public function getGenerateImages(Request $request, ImageRepository $imageRepository) {
        set_time_limit(0);
        /*$path = "/home/www/pavelrybakov/public/images";
        $files = scandir($path);
        $startFrom = 1462986361;
        $images = [];
        foreach($files as $file) {
            if ($file != '.' && $file != '..') {
                $time = filemtime($path . '/' . $file);
                if ($time > $startFrom) {
                    $images[] = '/images/' . $file;
                }
            }
        }

        return view('image.generated', compact('images'));*/


        $imageDir = $request->get('path');
        if (!$imageDir) {
            abort(404);
        }
        $images = [];
        $path = "/home/www/pavelrybakov/public/images/" . $imageDir;
        $files = scandir($path);
        foreach($files as $file) {
            if ($file != '.' && $file != '..') {
                $uploadedFile = new UploadedFile($path . '/' . $file, $file, null, null, null, true);
                if ($uploadedFile->isValid()) {
                    $images[] = $imageRepository->storeImage($uploadedFile);
                }
            }
        }

        return view('image.generated', compact('images'));
    }
}
