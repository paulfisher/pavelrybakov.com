<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Requests\ArticleRequest;
use App\Repositories\ImageRepository;
use Auth;
use Carbon\Carbon;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use League\Flysystem\Directory;

class ArticleController extends Controller
{

    public function __construct() {
        $this->middleware('auth', ['except' => ['show', 'index', 'random', 'travelSecurity']]);
    }

    public function index() {

        $articles = Article::latest('created_at')->published()->get();

        return view('article.index', compact('articles'));
    }

    public function show(Article $article) {
        return view('article.show', compact('article'));
    }

    public function random() {
        /** @var Article $article */
        $article = Article::orderByRaw("RAND()")->firstOrFail();
        return redirect('/article/' . $article->slugOrId());
    }

    public function create() {
        return view('article.create', compact('tags'));
    }

    public function store(ArticleRequest $request, ImageRepository $imageRepository) {
        $this->createArticle($request, $imageRepository);

        flash()->overlay('Your article has been created', 'Good job')->important();

        return redirect('/');
    }

    public function edit(Article $article) {
        return view('article.edit', compact('article'));
    }

    public function update(Article $article, ArticleRequest $request, ImageRepository $imageRepository) {
        $article->update($request->all());

        if (Input::hasFile('image') && Input::file('image')->isValid()) {
            $image = Input::file('image');
            $imageSrc = $imageRepository->storeImage($image, 600);
            $article->main_img = $imageSrc;
            $article->save();
        }

        $this->syncTags($article, $request->input('tag_list', []));

        return redirect('/');
    }

    public function travelSecurity() {
        return view('article.travel-security');
    }

    private function syncTags(Article $article, array $tags) {
        $article->tags()->sync($tags);
    }

    private function createArticle(ArticleRequest $request, ImageRepository $imageRepository) {
        $article = Auth::user()
            ->articles()
            ->create($request->all());
        $article->slug = self::slugIt($article->title);
        $article->save();

        if (Input::hasFile('image') && Input::file('image')->isValid()) {
            $image = Input::file('image');
            $imageSrc = $imageRepository->storeImage($image, 600);
            $article->main_img = $imageSrc;
            $article->save();
        }

        $this->syncTags($article, $request->input('tag_list', []));

        return $article;
    }

//    public function listImages(Article $article) {
//        /** @var Filesystem $fs */
//        $fs = Storage::disk('local');
//
//        $imagePath = 'images/article_' . $article->id . '/thumb';
//
//        if (Request::has('refresh_images')) {
//            set_time_limit(0);
//            $this->refreshImages($article);
//        }
//
//        $files = $fs->allFiles($imagePath);
//        $result = [];
//        foreach ($files as $file) {
//            $result[] = '/' . $this->publishImage($file);
//        }
//        return $result;
//    }
//
//    private function publishImage($file) {
//        $publicAsset = public_path() . '/' . $file;
//        if (!File::exists($publicAsset)) {
//            File::makeDirectory(dirname($publicAsset), 0775, true, true);
//            File::copy(storage_path() . '/app/' . $file, $publicAsset);
//        }
//        return $file;
//    }
//
//    private function refreshImages(Article $article) {
//        /** @var Filesystem $fs */
//        $fs = Storage::disk('local');
//
//        if (!$fs->exists('images')) {
//            $fs->makeDirectory('images');
//        }
//
//        $originalFolder = 'images/article_' . $article->id;
//        $resizedFolder = $originalFolder . '/thumb';
//        if (!$fs->exists($resizedFolder)) {
//            $fs->makeDirectory($resizedFolder);
//        }
//
//        foreach ($fs->allFiles($originalFolder) as $originalFile) {
//
//            $extension = strtolower(File::extension($originalFile));
//            $name = File::name($originalFile);
//
//            $resizedFilePath = $resizedFolder . '/' . $name . '.' . $extension;
//
//            if ($fs->exists($resizedFilePath)) continue;
//
//            switch ($extension) {
//                case 'jpg':
//                case 'png':
//                    $resized = $this->resizeImage($fs->get($originalFile))->encode();
//                    $fs->put($resizedFilePath, $resized);
//                    break;
//            }
//        }
//    }
//
//    private function resizeImage($originalImage) {
//        $image = Image::make($originalImage);
//        return $image->resize(800, 600);
//    }

    public static function translit($str) {
        static $converter = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v','г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh','з' => 'z','и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n','о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u','ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch','ш' => 'sh','щ' => 'sch','ь' => '',  'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu','я' => 'ya','А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E','Ё' => 'E', 'Ж' => 'Zh','З' => 'Z',
            'И' => 'I', 'Й' => 'Y', 'К' => 'K','Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R','С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C','Ч' => 'Ch','Ш' => 'Sh','Щ' => 'Sch',
            'Ь' => '',  'Ы' => 'Y', 'Ъ' => '','Э' => 'E', 'Ю' => 'Yu','Я' => 'Ya',
        );
        return strtr($str, $converter);
    }


    public static function slugIt($text) {
        $text = self::translit($text);

        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT//IGNORE', $text);

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        if (empty($text)) {
            return null;
        }

        return $text;
    }

}
