<?php

Route::get('/', 'PageController@index');
Route::get('blog', 'ArticleController@index');
Route::get('travel-security', 'ArticleController@travelSecurity');

Route::get('travel', 'PageController@travel');
Route::get('rafting', 'PageController@rafting2016');

Route::resource('article', 'ArticleController');
Route::get('random', 'ArticleController@random');

Route::get('tag/{tag}', 'TagController@show');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::get('image/upload', 'ImageController@getUpload');
Route::get('image/generate', 'ImageController@getGenerateImages');
Route::post('image/upload', 'ImageController@postUpload');